const users = [{
  id: 1,
  name: 'John',
  email: 'johnson@mail.com',
  age: 25,
  parent_address: 'USA',
  relationship:'mother'
},
{
  id: 1,
  name: 'John',
  email: 'johnson@mail.com',
  age: 25,
  parent_address: 'Spain',
  relationship:'father'
},
{
  id: 2,
  name: 'Mark',
  email: 'mark@mail.com',
  age: 28,
  parent_address: 'France',
  relationship:'father'
},
{
  id: 3,
  name: 'jamie',
  email: 'jamie@mail.com',
  age: 28,
  parent_address: 'France',
  relationship:'father'
},
{
  id: 3,
  name: 'jamie',
  email: 'jamie@mail.com',
  age: 28,
  parent_address: 'France',
  relationship:'mother'
},
{
  id: 3,
  name: 'jamie',
  email: 'jamie@mail.com',
  age: 28,
  parent_address: 'France',
  relationship:'grandmother'
}
]

const groupedUser = []

users.reduce((acc, user) => {
if (!acc[user.id]) {
  acc[user.id] = {
    id: user.id,
    name: user.name,
    age: user.age,
    parents: [{
      relationship: user.relationship,
      address: user.parent_address
    }]
  }
  groupedUser.push(acc[user.id])
} else {
  acc[user.id].parents.push({
    relationship: user.relationship,
    address: user.parent_address
  })
}

return acc
}, {})


console.log(groupedUser)
